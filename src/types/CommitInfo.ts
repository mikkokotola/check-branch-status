export type CommitInfo = {
    repo: string;
    date: Date;
    author: string;
    commitHashShort: string;
    commitMessage: string;
};