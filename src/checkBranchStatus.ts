/**
 * Module to check the status of branches.
 * Mikko Kotola, 2022
 */

import simpleGit, { SimpleGitOptions, LogResult, SimpleGit } from 'simple-git';
import yargs from 'yargs';
import fs from 'fs';

import { CommitInfo } from './types/CommitInfo';
import { OutputData } from './output/OutputData';

// Use ConsoleWriter. If you need to implement another output writer, just make
// it implement the Writer interface and swap it in here.
import { ConsoleWriter } from './output/consoleOutputWriter.js';
const writer = new ConsoleWriter();

// Configure here your project's git base address
const gitBaseAddress = 'bitbucket.org:mikkokotola';

const argv = yargs(process.argv.slice(2))
    .usage('Usage: $0 [options]')
    .example('$0 --branch origin/env/test', 'Check the status of branch env/test against base branch master in repos listed in file reposToCheck.txt')
    .example('$0 --branch origin/env/test --base origin/master --repos reposToCheck.txt', 'Check the status of branch env/test against base branch master in repos listed in file reposToCheck.txt')
    .alias('t', 'target')
    .nargs('t', 1)
    .describe('t', 'Name of target branch to check.')
    .default('t', 'origin/env/test')
    .alias('b', 'base')
    .nargs('b', 1)
    .describe('b', 'Name of base branch.')
    .default('b', 'origin/master')
    .alias('r', 'reposFile')
    .nargs('r', 1)
    .describe('r', 'File that contains a list of repo names to check, one per line. Repo folders must be daughters of current folder.')
    .default('r', 'reposToCheck.txt')
    .alias('s', 'skipFetch')
    .boolean(['s'])
    .describe('s', 'Skip fetching all remote branches.')
    .default('s', false)
    .help('h')
    .alias('h', 'help')
    .epilog('Git branch status checker, Mikko Kotola 2022, MIT License')
    .strict()
    .argv;

writer.title('Branch status checker');

const targetBranch: string = argv.target;
const baseBranch: string = argv.base;
const reposFile: string = argv.reposFile;
const skipFetchingRemoteBranches: boolean = argv.skipFetch;
writer.info(`  Target branch: ${targetBranch}`);
writer.info(`  Base branch: ${baseBranch}`);
writer.info(`  Repos file path: ${reposFile}`);
writer.info(`  Skip fetching remote branches: ${skipFetchingRemoteBranches}`);

let repos: string[];
try {
    repos = fs.readFileSync(reposFile, 'utf-8').split('\n');
} catch (e) {
    writer.error(`Problem with reading file ${reposFile}. Check that the file exists and is encoded in UTF-8.`);
    writer.error(e.stack);
    process.exit(1);
}

if (!skipFetchingRemoteBranches) {
    await fetchOrCloneRepos(targetBranch, baseBranch, repos, gitBaseAddress);
}

await checkBranches(targetBranch, baseBranch, repos);

async function checkBranches(targetBranch: string, baseBranch: string, repos: string[]) {
    // Map to contain non-merge commits diff betweeen the source and target branches
    const changeMap: Map<Date, CommitInfo[]> = new Map();
    // Map to contain latest merge commits into the target branch
    const latestMergeMap: Map<Date, CommitInfo[]> = new Map();
    const upToDateRepos: string[] = [];

    for (const repo of repos) {
        const git: SimpleGit = await initializeSimpleGitForRepo(repo);
        if (!git) continue;

        try {
            const mergeCommits = await git.log([`${targetBranch}...${baseBranch}`, "--pretty='%C(auto)%aI;%an;%h;%s'", "--merges"]);
            if (mergeCommits?.all[0]?.hash) {
                const mergeCommitInfos = await getCommitsFromSimpleGitLogResult(repo, mergeCommits);
                const latestMergeTime = mergeCommitInfos[0].date;
                latestMergeMap.has(latestMergeTime) ? latestMergeMap.set(latestMergeTime, latestMergeMap.get(latestMergeTime).concat(mergeCommitInfos.slice(0, 1))) : latestMergeMap.set(latestMergeTime, mergeCommitInfos.slice(0, 1));
            }

            const nonMergeCommits = await git.log([`${targetBranch}...${baseBranch}`, "--pretty='%C(auto)%aI;%an;%h;%s'", "--no-merges"]);
            if (nonMergeCommits?.all[0]?.hash) {
                const nonMergeCommitInfos = await getCommitsFromSimpleGitLogResult(repo, nonMergeCommits);
                const latestNonMergeCommitTime = nonMergeCommitInfos[0].date;
                changeMap.has(latestNonMergeCommitTime) ? changeMap.set(latestNonMergeCommitTime, changeMap.get(latestNonMergeCommitTime).concat(nonMergeCommitInfos)) : changeMap.set(latestNonMergeCommitTime, nonMergeCommitInfos);
            } else {
                upToDateRepos.push(repo);
            }

        }
        catch (e) {
            if (e.message.includes('unknown revision or path not in the working tree')) {
                writer.error(`Did not find branches ${targetBranch} or ${baseBranch} in ${repo}. Check that branches exist. Omitting results for this repo.`);
            } else {
                writer.error(`Error during processing of repo ${repo}`);
                writer.error(e.stack);
            }

        }
    }

    writer.outPutResults({targetBranch, baseBranch, latestMergeMap, changeMap, upToDateRepos});
}

async function getCommitsFromSimpleGitLogResult(repo: string, logResult: LogResult): Promise<CommitInfo[]> {
    const commitsString = logResult.all[0].hash;
    const commits = commitsString.split('\n').map(x => x.slice(1, -1));
    const commitInfos: CommitInfo[] = commits.map(commitString => {
        const commitArray = commitString.split(';');
        const commitInfo: CommitInfo = {
            date: new Date(commitArray[0]),
            repo: repo,
            author: commitArray[1],
            commitHashShort: commitArray[2],
            commitMessage: commitArray[3]
        };
        return commitInfo;
    });

    return commitInfos;
}

async function fetchOrCloneRepos(targetBranch: string, baseBranch: string, repos: string[], gitBaseAddress : string){
    let fetchOrClonePromises : Promise<SimpleGit>[] = [];
    for (const repo of repos) {
        fetchOrClonePromises.push(fetchOrCloneRepo(repo, gitBaseAddress));
    }
    const bar = await Promise.all(fetchOrClonePromises);
}

async function fetchOrCloneRepo(repo: string, gitBaseAddress: string): Promise<SimpleGit> {
    const options: Partial<SimpleGitOptions> = {
        baseDir: `../${repo}`,
    };
    let git: SimpleGit;
    let tryCloning: boolean = false;
    try {
        git = simpleGit(options);
        writer.info(`Fetching all commits for repo ${repo}`);
        await git.fetch(['--all', '--quiet']);
    } catch (e) {
        if (e.message.includes('Cannot use simple-git on a directory that does not exist')) {
            writer.info(`Did not find repo ${repo} at ../${repo}.`);
            tryCloning = true;
            git = simpleGit();
        } else {
            writer.error(`Error during initializing git or fetching for repo ${repo}.`);
            writer.error(e.stack);
        }
        //writer.error(e.stack);
    }

    if (tryCloning) {
        const targetFolder = `../${repo}`;
        const repoAddress = `git@${gitBaseAddress}/${repo}.git`;
        writer.info(`Proceeding to try cloning repo to directory ${targetFolder}.`);
        try {
            await git.clone(repoAddress, targetFolder);
            writer.info(`Successfully cloned repo to directory ${targetFolder}.`);
        } catch (e) {
            writer.error(`Error during cloning git repo ${repoAddress} to folder ${repo} .`);
            writer.error(e.stack);
            return null;
        }
    }
    return git;
}

async function initializeSimpleGitForRepo(repo: string): Promise<SimpleGit> {
    const options: Partial<SimpleGitOptions> = {
        baseDir: `../${repo}`,
    };
    let git: SimpleGit = null;
    try {
        git = simpleGit(options);
    } catch (e) {
        if (e.message.includes('Cannot use simple-git on a directory that does not exist')) {
            writer.info(`Did not find repo ${repo} at ../${repo}.`);
        } else {
            writer.error(`Error during initializing git for repo ${repo}.`);
            writer.error(e.stack);
        }
        //writer.error(e.stack);
    }
    return git;
}
