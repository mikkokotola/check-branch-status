import { CommitInfo } from '../types/CommitInfo';

export interface OutputData {
  targetBranch: string;
  baseBranch: string;
  latestMergeMap: Map<Date, CommitInfo[]>;
  changeMap: Map<Date, CommitInfo[]>;
  upToDateRepos: string[];
}