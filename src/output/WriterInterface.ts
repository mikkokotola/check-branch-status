import { OutputData } from "./OutputData";

export interface Writer {
    error(output: string) : void;
    info(output: string) : void;
    title(output: string) : void;
    success(output: string) : void;
    outPutResults(outputData: OutputData) : void;
}