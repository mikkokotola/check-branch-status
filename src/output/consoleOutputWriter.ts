import clc from 'cli-color';
import Table from 'cli-table';
import moment from 'moment';

import { OutputData } from './OutputData';
import { Writer } from './WriterInterface';

// Color definitions for console output
const errorCol = clc.red.bold;
//const warn = clc.yellow;
const titleCol = clc.blue.bold;
const infoCol = clc.white;
const successCol = clc.green;

export class ConsoleWriter implements Writer {
    error(output: string) {
        console.log(errorCol(output));
    }

    info(output: string) {
        console.log(infoCol(output));
    }

    title(output: string) {
        console.log(titleCol(output));
    }

    success(output: string) {
        console.log(successCol(output));
    }

    outPutResults(outputData: OutputData) {
        console.log(titleCol(`\nLatest merges per repo into ${outputData.targetBranch}:`));
        const latestMergeTable = new Table({
            head: ['When', 'Repo', 'Author', 'Commit', 'Commit message'],
            colWidths: [20, 40, 20, 10, 100]
        });
        for (const key of Array.from(outputData.latestMergeMap.keys()).sort((a, b) => b.getTime() - a.getTime())) {
            const mergeComm = outputData.latestMergeMap.get(key)[0];
            const relativeTime = moment(mergeComm.date.toISOString().slice(0, 19), "YYYY-MM-DDTHH:mm:ss").fromNow();
            latestMergeTable.push([relativeTime, mergeComm.repo, mergeComm.author, mergeComm.commitHashShort, mergeComm.commitMessage]);
        }
        console.log(latestMergeTable.toString());

        const changeTable = new Table({
            head: ['When', 'Repo', 'Author', 'Commit', 'Commit message'],
            colWidths: [20, 40, 20, 10, 100]
        });

        console.log(titleCol(`Branch ${outputData.targetBranch} is following commits behind branch ${outputData.baseBranch}:`));
        for (const key of Array.from(outputData.changeMap.keys()).sort((a, b) => b.getTime() - a.getTime())) {
            const commits = outputData.changeMap.get(key);
            commits.forEach(commit => {
                const relativeTime = moment(commit.date.toISOString().slice(0, 19), "YYYY-MM-DDTHH:mm:ss").fromNow();
                changeTable.push([relativeTime, commit.repo, commit.author, commit.commitHashShort, commit.commitMessage]);
            });
        }
        console.log(changeTable.toString());

        console.log(titleCol(`Branch ${outputData.targetBranch} is up to date with branch ${outputData.baseBranch} for following repos:`));
        for (const repo of outputData.upToDateRepos) {
            console.log(successCol(repo));
        }
        console.log();
    }
}
