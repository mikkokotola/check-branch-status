# Environment checker
A CLI utility to check the status of a branch against the status of a base branch. Can be used e.g. to check if and environment branch is up-to-date with master.

## How to install
- Prerequisite: node and npm installed
- In repo root, run `npm install`

## How to use
- Clone the repos you want to check in sister folders of this utility repo
```
repos--repo1
    |--repo2
    |--repo3
    |--branch-status-checker
```
- List the repos you want to check in the file *reposToCheck.txt*. One per line. E.g.:
```
repo1
repo2
repo3
```
- Run the tool with command `npm run check -- --target origin/env/prod` to check the status of the remote branch *env/prod* against remote branch *master* (the default base branch).
## Commands
- Compile and run: `npm run check`
- Lint: `npm run lint`
- Fix linting errors: `npm run fixlint`

## Examples
- `npm run check`: check the status of the remote branch *env/test* (the default target branch) against remote branch *master* (the default base branch).
- `npm run check -- --target origin/env/prod`: check the status of the remote branch *env/prod* against remote branch *master* (the default base branch).
- `npm run check -- --target origin/env/prod --skipFetch`: check the status of the remote branch *env/prod* against remote branch *master* (the default base branch), and skip fetching remote branches (they have been recently fetched).
- `npm run check -- --target origin/env/staging --base origin/main --reposFile ../repos.txt`: check the status of the remote branch *env/staging* against remote branch *main* for repos listed in the file *../repos.txt*.

## Options
- **-t, --target** (*default: origin/env/test*): what target branch to check
- **-b, --base** (*default: origin/master*): what base branch to check against
- **-r, --reposFile** (*default: reposToCheck.txt*): path to file with a list of repos to check. The file must be encoded UTF-8 and contain repo names (one per line). The repos must be located in sister folders of this repo (repo-status-checker).
- **-s, --skipFetch** (*default: false*): Skip fetching all remote branches.